# Laravel 5 Swagger definition

Partial port of [adonis-swagger](https://www.npmjs.com/package/adonis-swagger) with some work done from [DarkaOnLine/L5-Swagger](https://github.com/DarkaOnLine/L5-Swagger)
to define swagger definition in yaml files in place of defining them as class decorator

>headup this library only support swagger api v2 for know !

## what's included
   * yaml glob file to exposed swagger.json file
   * swagger ui correctly redirected to it

## usage

`composer require goopil/yml-swagger-l5`

for laravel < 5.5

add `Goopil\YmlSwagger\YmlSwaggerProvider` to `config/app.php`

## assets

* all `php artisan vendor:publish --provider=Goopil\YmlSwagger\YmlSwaggerProvider`
* config only `php artisan vendor:publish --provider=Goopil\YmlSwagger\YmlSwaggerProvider --tag=config`

## adjust the configuration
usually i add this structure to define my endpoints
* `route/swagger/dto` to define the models (dto as data transfer object)
* `route/swagger/api` to define the endpoints.

> you can add reference between files as all the yml file are concatenated before serving

### example
> user dto

`route/swagger/dto/user.yml`
```yml
definitions:

  NewUser:
    type: object
    required:
      - username
      - email
      - password
    properties:
      username:
        type: string
      email:
        type: string
      password:
        type: string
        format: password

  UpdateUser:
    type: object
    properties:
      username:
        type: string

  User:
    allOf:
      - $ref: '#/definitions/NewUser'
      - type: object
        properties:
         id:
           type: uint
           readOnly: true
         created_at:
           type: string
           format: date-time
           readOnly: true
         updated_at:
           type: string
           format: date-time
           readOnly: true
```
> user show endpoint
`route/swagger/api/user.yml`
```yaml
paths:
  /users/{id}:
    get:
      tags:
      - Users
      operationId: userShow
      security:
      - JWT: []
      parameters:
      - name: id
      in: path
      type: string
      responses:
        200:
          description: Requested User
          schema:
            $ref: '#/definitions/User'
```

# Contribution

Every contribution are welcome from bug report to new idea and obviously merge request.
