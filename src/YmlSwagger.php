<?php

namespace Goopil\YmlSwagger;

use Illuminate\Config\Repository as Config;
use Symfony\Component\Yaml\Yaml;

class YmlSwagger
{
    /**
     * @var Config
     */
    protected $config;
    /**
     * Definition file paths
     *
     * @var array
     */
    protected $paths = [];

    /**
     * Parsed definition
     *
     * @var array
     */
    protected $definition;

    /**
     * YmlSwagger constructor.
     *
     * @param Illuminate\Config\Repository $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config->get('ymlSwagger');
        $this->paths  = $this->parse($this->config['files']);
    }

    /**
     * get all file path recursively
     *
     * @param $paths
     *
     * @return mixed
     */
    protected function parse($paths)
    {
        return array_reduce($paths, function ($result, $path) {
            return array_merge($result, glob($path));
        }, []);
    }

    /**
     * extract yaml config from defined paths
     *
     * @return array
     */
    public function format()
    {
        $content = array_map(function ($path) {
            return Yaml::parse(file_get_contents($path));
        }, $this->paths);

        $this->definition = count($content) > 0 ?
            array_merge_recursive(...$content) :
            [];

        return array_merge(
            $this->formatHeader(),
            $this->definition
        );
    }

    protected function formatHeader()
    {
        return [
            'info'                => [
                'title'   => $this->config['title'],
                'version' => $this->config['version'],
            ],
            'basepath'            => $this->config['basePath'],
            'securityDefinitions' => $this->config['security'],
            "swagger"             => "3.0",
        ];
    }
}
