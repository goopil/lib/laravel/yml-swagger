<?php

namespace Goopil\YmlSwagger;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class YmlSwaggerProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @param Router $router
     *
     * @return void
     */
    public function boot(Router $router)
    {
        $this->publishes([
            __DIR__ . '/../config/ymlSwagger.php' => config_path('ymlSwagger.php'),
        ], 'config');

        $this->loadViewsFrom(__DIR__ . '/../views', 'ymlSwagger');

        $this->publishes([
            __DIR__ . '/../views' => resource_path('views/vendor/ymlSwagger'),
        ], 'view');


        require __DIR__ . '/routes.php';
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/ymlSwagger.php', 'ymlSwagger');

        require_once 'helper.php';
    }
}
