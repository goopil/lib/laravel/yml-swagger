<?php


$router->group([
    'namespace'  => 'Goopil\YmlSwagger\Controllers',
    'middleware' => 'web'
], function ($r) {
    /**
     * ui
     */
    $r->get(
        config('ymlSwagger.routing.ui.path'),
        'SwaggerController@ui'
    )
      ->name(config('ymlSwagger.routing.ui.name'))
      ->middleware(config('ymlSwagger.routing.ui.middleware', []));

    /**
     * ui assets
     */
    $r->get(
        config('ymlSwagger.routing.ui.path') . '/assets/{asset}',
        'SwaggerController@assets'
    )
      ->name(config('ymlSwagger.routing.ui.name') . '.assets')
      ->middleware(config('ymlSwagger.routing.assets.middleware', []));

    /**
     * oauth2 callback
     */
    $r->get(
        config('ymlSwagger.routing.definition.path'),
        'SwaggerController@definition'
    )
      ->name(config('ymlSwagger.routing.definition.name'))
      ->middleware(config('ymlSwagger.routing.definition.middleware', []));

    /**
     * swagger definition
     */
    $r->get(
        config('ymlSwagger.routing.oauth2.path'),
        'SwaggerController@oauth2Callback'
    )
      ->name(config('ymlSwagger.routing.oauth2.name'))
      ->middleware(config('ymlSwagger.routing.oauth2.middleware', []));
});
