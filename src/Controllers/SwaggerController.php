<?php

namespace Goopil\YmlSwagger\Controllers;

use Goopil\YmlSwagger\YmlSwagger;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SwaggerController
{
    /**
     * Dump api-docs.json content endpoint.
     *
     * @param Response $response
     *
     * @param YmlSwagger $swagger
     *
     * @return Response
     */
    public function definition(YmlSwagger $swagger)
    {
        $content = $swagger->format();

        return response()->json($content);
    }


    /**
     * Display Oauth2 callback pages.
     *
     * @param File $file
     *
     * @return string
     */
    public function oauth2Callback(File $file)
    {
        return $file->get(swagger_ui_dist_path('oauth2-redirect.html'));
    }

    /**
     * Display Swagger API page.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function ui(Request $request)
    {
        return view('ymlSwagger::ui', [
            'secure'           => $request->secure(),
            'urlToDocs'        => route(config('ymlSwagger.routing.definition.name')),
            'operationsSorter' => config('l5-ymlSwagger.operations_sort'),
            'configUrl'        => config('l5-ymlSwagger.additional_config_url'),
            'validatorUrl'     => config('l5-ymlSwagger.validator_url'),
        ]);
    }

    public function assets($asset)
    {
        $path = swagger_ui_dist_path($asset);

        return (new Response(
            file_get_contents($path), 200, [
                'Content-Type' => (pathinfo($asset))['extension'] == 'css' ? 'text/css' : 'application/javascript',
            ]
        ));
    }
}
