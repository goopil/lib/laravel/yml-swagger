<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Swagger Information
    | Please use Swagger 2 Spesification Docs
    | https://swagger.io/docs/specification/2-0/basic-structure/
    |--------------------------------------------------------------------------
    */

    // enable or disable route '/swagger.json'
    'enable' => true,

    'title'    => 'Laravel 💘 Swagger',
    'version'  => '1.0.0',
    'basePath' => '/',

    /*
|--------------------------------------------------------------------------
| API security definitions. Will be generated into documentation file.
|--------------------------------------------------------------------------
*/
    'security' => [
        /*
        |--------------------------------------------------------------------------
        | Examples of Security definitions
        |--------------------------------------------------------------------------
        */
        /*
        'api_key_security_example' => [ // Unique name of security
            'type' => 'apiKey', // The type of the security scheme. Valid values are "basic", "apiKey" or "oauth2".
            'description' => 'A short description for security scheme',
            'name' => 'api_key', // The name of the header or query parameter to be used.
            'in' => 'header', // The location of the API key. Valid values are "query" or "header".
        ],
        'oauth2_security_example' => [ // Unique name of security
            'type' => 'oauth2', // The type of the security scheme. Valid values are "basic", "apiKey" or "oauth2".
            'description' => 'A short description for oauth2 security scheme.',
            'flow' => 'implicit', // The flow used by the OAuth2 security scheme. Valid values are "implicit", "password", "application" or "accessCode".
            'authorizationUrl' => 'http://example.com/auth', // The authorization URL to be used for (implicit/accessCode)
            //'tokenUrl' => 'http://example.com/auth' // The authorization URL to be used for (password/application/accessCode)
            'scopes' => [
                'read:projects' => 'read your projects',
                'write:projects' => 'modify projects in your account',
            ]
        ],
        */
        /* Open API 3.0 support
        'passport' => [ // Unique name of security
            'type' => 'oauth2', // The type of the security scheme. Valid values are "basic", "apiKey" or "oauth2".
            'description' => 'Laravel passport oauth2 security.',
            'in' => 'header',
            'scheme' => 'https',
            'flows' => [
                "password" => [
                    "authorizationUrl" => config('app.url') . '/oauth/authorize',
                    "tokenUrl" => config('app.url') . '/oauth/token',
                    "refreshUrl" => config('app.url') . '/token/refresh',
                    "scopes" => []
                ],
            ],
        ],
        */
    ],

    // Path to the API docs
    // Sample usage
    // 'files' => [
    //    'docs/**/*.yml',    // load recursive all .yml file in docs directory
    //    'docs/**/*.php',     // load recursive all .php classes in docs directory
    // ]
    'files'    => [
        base_path("routes/swagger/*/*.yml"),
    ],

    'routing'               => [
        'ui'         => [
            'path'       => 'api/explorer',
            'name'       => 'swagger.ui',
            'middleware' => []
        ],
        'oauth2'     => [
            'path'       => 'api/explorer/auth',
            'name'       => 'swagger.auth',
            'middleware' => []
        ],
        'definition' => [
            'path'       => 'swagger.json',
            'name'       => 'swagger.definition',
            'middleware' => []
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Configs plugin allows to fetch external configs instead of passing them to SwaggerUIBundle.
    | See more at: https://github.com/swagger-api/swagger-ui#configs-plugin
    |--------------------------------------------------------------------------
    */
    'additional_config_url' => null,
    /*
    |--------------------------------------------------------------------------
    | Apply a sort to the operation list of each API. It can be 'alpha' (sort by paths alphanumerically),
    | 'method' (sort by HTTP method).
    | Default is the order returned by the server unchanged.
    |--------------------------------------------------------------------------
    */
    'operations_sort'       => env('L5_SWAGGER_OPERATIONS_SORT', null),
    /*
    |--------------------------------------------------------------------------
    | Uncomment to pass the validatorUrl parameter to SwaggerUi init on the JS
    | side.  A null value here disables validation.
    |--------------------------------------------------------------------------
    */
    'validator_url'         => null,
];
